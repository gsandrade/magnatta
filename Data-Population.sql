use EasyPizza
go

--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = '__MigrationHistory')) BEGIN drop table __MigrationHistory END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'Addresses')) BEGIN drop table Addresses END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'Logins')) BEGIN drop table Logins END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'Phones')) BEGIN drop table Phones END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'Products')) BEGIN drop table Products END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'ProductSizes')) BEGIN drop table ProductSizes END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'ProductTags')) BEGIN drop table ProductTags END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'Sizes')) BEGIN drop table Sizes END
--IF(EXISTS(SELECT 1 FROM SYS.TABLES where name = 'Users')) BEGIN drop table Users END 
--GO


--INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width)
--VALUES(NEWID(), 'P', 17, 20, 46, 14, 66, 51)
--INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width)
--VALUES(NEWID(), 'M', 19, 22, 48, 16, 68, 53)
--INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width)
--VALUES(NEWID(), 'G', 21, 24, 50, 18, 70, 55)
--INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width)
--VALUES(NEWID(), 'GG', 23, 26, 52, 20, 72, 57)

--declare @pai uniqueidentifier set @pai = NEWID()
--declare @masculino uniqueidentifier set @masculino = NEWID()
--declare @feminino uniqueidentifier set @feminino = NEWID()
--declare @infantil uniqueidentifier set @infantil = NEWID()

--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (@pai, null, 'Pe�as',							1, 'javascript:', '', 5, '')

--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (@masculino, @pai, 'Masculino',					1, 'javascript:', '', 5, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (@feminino, @pai, 'Feminino',					1, 'javascript:', '', 10, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (@infantil, @pai, 'Infantil',					1, 'javascript:', '', 15, '')

--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'T-Shirts',				1, '', '', 5, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Polos',					1, '', '', 10, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Bermudas',				1, '', '', 15, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Camisas',				1, '', '', 20, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Acess�rios',				1, '', '', 25, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Casacos',				1, '', '', 30, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Cal�ados',				1, '', '', 35, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Cal�as',					1, '', '', 40, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @masculino, 'Cart�es de Presente',	1, '', '', 45, '')

--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'T-Shirts',				1, '', '', 5, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Polos',					1, '', '', 10, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Bermudas',				1, '', '', 15, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Camisas',					1, '', '', 20, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Acess�rios',				1, '', '', 25, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Casacos',					1, '', '', 30, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Cal�ados',				1, '', '', 35, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Cal�as',					1, '', '', 40, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @feminino, 'Cart�es de Presente',		1, '', '', 45, '')

--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'T-Shirts',				1, '', '', 5, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Polos',					1, '', '', 10, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Bermudas',				1, '', '', 15, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Camisas',					1, '', '', 20, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Acess�rios',				1, '', '', 25, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Casacos',					1, '', '', 30, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Cal�ados',				1, '', '', 35, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Cal�as',					1, '', '', 40, '')
--INSERT INTO Menus (Id, IdPai, Nome, Visivel, Href, Descricao, Ordem, Target) VALUES (NEWID(), @infantil, 'Cart�es de Presente',		1, '', '', 45, '')