﻿using Magnatta.Biz;
using Magnatta.Biz.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Magnatta.Controllers
{
	public class ProdutoController : Controller
	{
		private ProductBiz _biz;
		private int GRID_SLICE = 3;
		public ProdutoController()
		{
			_biz = new ProductBiz();
		}
		public ActionResult Index(Guid id)
		{
			var product = _biz.GetProduct(id);
			return View(product);
		}
		public ActionResult Lista()
		{
			var products = GetProducts();
			return View(products);
		}
		public ActionResult GetProductsByName(string name)
		{
			var products = GetProducts(name);
			return PartialView("_partialProducts", products);
		}
		private IEnumerable<IEnumerable<Product>> GetProducts(string name = "")
		{
			var products = _biz.GetProductsInStock(name);
			var list = ToGrid(products, GRID_SLICE);
			return list;
		}
		private static IEnumerable<IEnumerable<Product>> ToGrid(IEnumerable<Product> products, int sliceSize)
		{
			while (products.Any())
			{
				yield return products.Take(sliceSize);
				products = products.Skip(sliceSize);
			}
		}
		public ActionResult Auxiliar()
		{
			return View();
		}
	}
}