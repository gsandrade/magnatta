﻿using Magnatta.Biz;
using Magnatta.Biz.VO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Magnatta.Controllers
{
	public class ContaController : Controller
	{
		private UserBiz _biz;
		public ContaController()
		{
			_biz = new UserBiz();
		}
		// GET: Conta
		public ActionResult Index()
		{
			return View();
		}

		[Authorize]
		public ActionResult Login(string email, string password)
		{
			return View();
		}

		public ActionResult CreateAccount(User user)
		{
			_biz.SaveUser(user);
			return View();
		}

		private bool Validate(User user)
		{
			var result = true;
			if (string.IsNullOrEmpty(user.FirstName))
			{
				result = false;
			}
			if (string.IsNullOrEmpty(user.LastName))
			{
				result = false;
			}
			if (user.Sex == 0)
			{
				result = false;
			}
			DateTime birthDate = DateTime.MinValue;
			DateTime.TryParse(user.BirthDate, out birthDate);
			if (birthDate == DateTime.MinValue)
			{
				result = false;
			}
			if (string.IsNullOrEmpty(user.Email))
			{
				result = false;
			}
			if (string.IsNullOrEmpty(user.Password))
			{
				result = false;
			}
			return result;
		}
	}
}