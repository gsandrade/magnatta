﻿using Magnatta.Dao.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Generic.Repository
{
	public abstract class GenericRepository<T> : IRepository<T> where T : GenericEntity
	{
		public Context.Context Ctx = new Dao.Context.Context();

		public abstract DbSet<T> GetContext();
		public KeyValuePair<Guid, string> Create(T model)
		{
			try
			{
				GetContext().Add(model);
				Ctx.SaveChanges();
				return new KeyValuePair<Guid, string>(model.Id, string.Empty);
			}
			catch (Exception e)
			{
				return new KeyValuePair<Guid, string>(new Guid(), e.Message);
			}
		}
		public KeyValuePair<bool, string> Alter(T model)
		{
			try
			{
				var original = GetContext().Find(model.Id);
				original = model;
				this.Ctx.SaveChanges();
				return new KeyValuePair<bool, string>(true, string.Empty);
			}
			catch (Exception e)
			{
				return new KeyValuePair<bool, string>(false, e.Message);
			}
		}
		public T Find(Guid id)
		{
			try
			{
				return GetContext().Where(x => x.Id == id).FirstOrDefault();
			}
			catch
			{
				return null;
			}
		}
		public T Find(Expression<Func<T, bool>> query)
		{
			try
			{
				return GetContext().Where(query).FirstOrDefault();
			}
			catch
			{
				return null;
			}
		}
		public IEnumerable<T> FindAll()
		{
			return GetContext();
		}
		public IEnumerable<T> FindAll(Expression<Func<T, bool>> query)
		{
			try
			{
				return GetContext().Where(query);
			}
			catch
			{
				return null;
			}
		}
		public bool Remove(Guid id)
		{
			try
			{
				var obj = this.Find(id);
				this.Ctx.Entry<T>(obj).State = EntityState.Deleted;
				GetContext().Remove(obj);
				this.Ctx.SaveChanges();
				return true;
			}
			catch
			{
				return false;
			}
		}

	}
}
