﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Generic.Repository
{
	public interface IRepository<T>
	{
		KeyValuePair<Guid, string> Create(T model);
		KeyValuePair<bool, string> Alter(T model);
		T Find(Guid id);
		T Find(Expression<Func<T, bool>> query);
		IEnumerable<T> FindAll(Expression<Func<T, bool>> query);
		bool Remove(Guid id);
	}
}
