﻿using Magnatta.Dao.Entities.User;
using Magnatta.Dao.Generic.Repository;
using System.Data.Entity;

namespace Magnatta.Dao.Repository
{
	public class PhoneRepository : GenericRepository<Phone>
	{
		public override DbSet<Phone> GetContext() { return Ctx.Phone; }
	}
}
