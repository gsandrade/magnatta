﻿using Magnatta.Dao.Entities.User;
using Magnatta.Dao.Generic.Repository;
using System.Data.Entity;

namespace Magnatta.Dao.Repository
{
	public class AddressRepository : GenericRepository<Address>
	{
		public override DbSet<Address> GetContext() { return Ctx.Address; }
	}
}
