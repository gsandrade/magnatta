﻿using Magnatta.Dao.Entities.User;
using Magnatta.Dao.Generic.Repository;
using System.Data.Entity;

namespace Magnatta.Dao.Repository
{
	public class UserRepository : GenericRepository<User>
	{
		public override DbSet<User> GetContext() { return Ctx.User; }
	}
}
