﻿using Magnatta.Dao.Entities.User;
using Magnatta.Dao.Generic.Repository;
using System.Data.Entity;

namespace Magnatta.Dao.Repository
{
	public class LoginRepository : GenericRepository<Login>
	{
		public override DbSet<Login> GetContext() { return Ctx.Login; }
	}
}
