﻿using Magnatta.Dao.Entities.Product;
using Magnatta.Dao.Generic.Repository;
using System.Data.Entity;

namespace Magnatta.Dao.Repository
{
	public class ProductTagRepository : GenericRepository<ProductTag>
	{
		public override DbSet<ProductTag> GetContext() { return Ctx.ProductTag; }
	}
}
