﻿using Magnatta.Dao.Entities.Product;
using Magnatta.Dao.Generic.Repository;
using System.Collections.Generic;
using System.Data.Entity;

namespace Magnatta.Dao.Repository
{
	public class ProductSizeRepository : GenericRepository<ProductSize>
	{
		public override DbSet<ProductSize> GetContext() { return Ctx.ProductSize; }

		public IEnumerable<ProductSize> GetAll()
		{
			return Ctx.ProductSize.Include("Product").Include("Size");
		}
	}
}
