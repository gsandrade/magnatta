﻿using Magnatta.Dao.Entities.Image;
using Magnatta.Dao.Entities.Product;
using Magnatta.Dao.Generic.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Magnatta.Dao.Repository
{
	public class ProductRepository : GenericRepository<Product>
	{
		public override DbSet<Product> GetContext() { return Ctx.Product; }

		public Product Get(Guid id)
		{
			var result = (
						from product in Ctx.Product
						where product.Id == id
						select new
						{
							Id = product.Id,
							Name = product.Name,
							Price = product.Price,
							CssSize = product.CssSize,
							Description = product.Description,
							Material = product.Material,
							Ordem = product.Ordem,
							Size = product.Size
						}).AsEnumerable()
						.Select(p => new Product
						{
							Id = p.Id,
							Name = p.Name,
							Price = p.Price,
							CssSize = p.CssSize,
							Description = p.Description,
							Material = p.Material,
							Ordem = p.Ordem,
							Size = p.Size
						}).FirstOrDefault();

			IEnumerable<Image> image =
					(from pi in Ctx.ProductImage
					 join im in Ctx.Image on pi.Image.Id equals im.Id
					 where pi.Product.Id == result.Id
					 select new
					 {
						 Id = im.Id,
						 Path = im.Path,
						 Title = im.Title
					 }).AsEnumerable().Select(i => new Image
					 {
						 Id = i.Id,
						 Path = i.Path,
						 Title = i.Title
					 }).ToList();
			result.Images = image;

			IEnumerable<Size> size =
			(from ps in Ctx.ProductSize
			 join si in Ctx.Size on ps.Size.Id equals si.Id
			 where ps.Product.Id == result.Id
			 select new
			 {
				 Id = si.Id,
				 Name = si.Name,
				 SleeveWidth = si.SleeveWidth,
				 SleeveLength = si.SleeveLength,
				 Shoulder = si.Shoulder,
				 Collar = si.Collar,
				 Height = si.Height,
				 Width = si.Width,
				 Ordem = si.Ordem
			 }).AsEnumerable().Select(s => new Size
				{
					Id = s.Id,
					Name = s.Name,
					SleeveWidth = s.SleeveWidth,
					SleeveLength = s.SleeveLength,
					Shoulder = s.Shoulder,
					Collar = s.Collar,
					Height = s.Height,
					Width = s.Width,
					Ordem = s.Ordem
				}).OrderBy(s => s.Ordem);
			result.Sizes = size;

			return result;
		}

		public IEnumerable<Product> GetAllInStock()
		{
			var result =
				(from products in Ctx.Product
				 join pSize in Ctx.ProductSize on products.Id equals pSize.Product.Id
				 select new
				 {
					 Id = products.Id,
					 Name = products.Name,
					 Price = products.Price,
					 CssSize = products.CssSize,
					 Description = products.Description,
					 Material = products.Material,
					 Ordem = products.Ordem,
					 Size = products.Size
				 })
				.Distinct()
				.AsEnumerable()
				.OrderBy(p => p.Ordem)
				.Select(p => new Product
				{
					Id = p.Id,
					Name = p.Name,
					Price = p.Price,
					CssSize = p.CssSize,
					Description = p.Description,
					Material = p.Material,
					Ordem = p.Ordem,
					Size = p.Size
				}).ToList();

			foreach (var item in result)
			{
				IEnumerable<Image> image =
					(from pi in Ctx.ProductImage
					 join im in Ctx.Image on pi.Image.Id equals im.Id
					 where pi.Product.Id == item.Id
					 select new
					 {
						 Id = im.Id,
						 Path = im.Path,
						 Title = im.Title
					 }).AsEnumerable().Select(i => new Image
					 {
						 Id = i.Id,
						 Path = i.Path,
						 Title = i.Title
					 }).ToList();
				item.Images = image;

				IEnumerable<Size> size =
				(from ps in Ctx.ProductSize
				 join si in Ctx.Size on ps.Size.Id equals si.Id
				 where ps.Product.Id == item.Id
				 select new
				 {
					 Id = si.Id,
					 Name = si.Name,
					 SleeveWidth = si.SleeveWidth,
					 SleeveLength = si.SleeveLength,
					 Shoulder = si.Shoulder,
					 Collar = si.Collar,
					 Height = si.Height,
					 Width = si.Width
				 }).AsEnumerable().Select(s => new Size
				 {
					 Id = s.Id,
					 Name = s.Name,
					 SleeveWidth = s.SleeveWidth,
					 SleeveLength = s.SleeveLength,
					 Shoulder = s.Shoulder,
					 Collar = s.Collar,
					 Height = s.Height,
					 Width = s.Width
				 }).ToList();
				item.Sizes = size;
			}
			return result;
		}

		public IEnumerable<Product> GetAllInStock(string name = "")
		{
			var result =
				(from products in Ctx.Product
				 join pSize in Ctx.ProductSize on products.Id equals pSize.Product.Id
				 where products.Name.ToUpper().Contains(name.ToUpper())
				 select new
				 {
					 Id = products.Id,
					 Name = products.Name,
					 Price = products.Price,
					 CssSize = products.CssSize,
					 Description = products.Description,
					 Material = products.Material,
					 Ordem = products.Ordem,
					 Size = products.Size
				 })
				.Distinct()
				.AsEnumerable()
				.OrderBy(p => p.Ordem)
				.Select(p => new Product
				{
					Id = p.Id,
					Name = p.Name,
					Price = p.Price,
					CssSize = p.CssSize,
					Description = p.Description,
					Material = p.Material,
					Ordem = p.Ordem,
					Size = p.Size
				}).ToList();

			foreach (var item in result)
			{
				IEnumerable<Image> image =
					(from pi in Ctx.ProductImage
					 join im in Ctx.Image on pi.Image.Id equals im.Id
					 where pi.Product.Id == item.Id
					 select new
					 {
						 Id = im.Id,
						 Path = im.Path,
						 Title = im.Title
					 }).AsEnumerable().Select(i => new Image
					 {
						 Id = i.Id,
						 Path = i.Path,
						 Title = i.Title
					 }).ToList();
				item.Images = image;

				IEnumerable<Size> size =
					(from ps in Ctx.ProductSize
					 join si in Ctx.Size on ps.Size.Id equals si.Id
					 where ps.Product.Id == item.Id
					 select new
					 {
						 Id = si.Id,
						 Name = si.Name,
						 SleeveWidth = si.SleeveWidth,
						 SleeveLength = si.SleeveLength,
						 Shoulder = si.Shoulder,
						 Collar = si.Collar,
						 Height = si.Height,
						 Width = si.Width
					 }).AsEnumerable().Select(s => new Size
					{
						Id = s.Id,
						Name = s.Name,
						SleeveWidth = s.SleeveWidth,
						SleeveLength = s.SleeveLength,
						Shoulder = s.Shoulder,
						Collar = s.Collar,
						Height = s.Height,
						Width = s.Width
					}).ToList();
				item.Sizes = size;
			}
			return result;
		}
	}
}
