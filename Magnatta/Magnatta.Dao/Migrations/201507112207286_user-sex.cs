namespace Magnatta.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usersex : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Sex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Sex");
        }
    }
}
