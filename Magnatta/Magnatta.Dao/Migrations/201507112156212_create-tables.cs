namespace Magnatta.Dao.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createtables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Street = c.String(),
                        Number = c.String(),
                        Complement = c.String(),
                        PostalCode = c.String(),
                        Neighborhood = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Path = c.String(),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Logins",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        User = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Phones",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        IDD = c.String(),
                        DDD = c.String(),
                        Number = c.String(),
                        Extension = c.String(),
                        Type = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        Material = c.String(),
                        Price = c.String(),
                        CssSize = c.String(),
                        Size = c.Int(nullable: false),
                        Ordem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Image_Id = c.Guid(),
                        Product_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Images", t => t.Image_Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .Index(t => t.Image_Id)
                .Index(t => t.Product_Id);
            
            CreateTable(
                "dbo.ProductSizes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        QuantityInStock = c.Int(nullable: false),
                        Product_Id = c.Guid(),
                        Size_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.Product_Id)
                .ForeignKey("dbo.Sizes", t => t.Size_Id)
                .Index(t => t.Product_Id)
                .Index(t => t.Size_Id);
            
            CreateTable(
                "dbo.Sizes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        SleeveWidth = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SleeveLength = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Shoulder = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Collar = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Height = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Width = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ordem = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductTags",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        Email = c.String(),
                        Login_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Logins", t => t.Login_Id)
                .Index(t => t.Login_Id);
            
            CreateTable(
                "dbo.UserAddresses",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Address_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Addresses", t => t.Address_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Address_Id)
                .Index(t => t.User_Id);
            
            CreateTable(
                "dbo.UserPhones",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Phone_Id = c.Guid(),
                        User_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Phones", t => t.Phone_Id)
                .ForeignKey("dbo.Users", t => t.User_Id)
                .Index(t => t.Phone_Id)
                .Index(t => t.User_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserPhones", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserPhones", "Phone_Id", "dbo.Phones");
            DropForeignKey("dbo.UserAddresses", "User_Id", "dbo.Users");
            DropForeignKey("dbo.UserAddresses", "Address_Id", "dbo.Addresses");
            DropForeignKey("dbo.Users", "Login_Id", "dbo.Logins");
            DropForeignKey("dbo.ProductSizes", "Size_Id", "dbo.Sizes");
            DropForeignKey("dbo.ProductSizes", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.ProductImages", "Product_Id", "dbo.Products");
            DropForeignKey("dbo.ProductImages", "Image_Id", "dbo.Images");
            DropIndex("dbo.UserPhones", new[] { "User_Id" });
            DropIndex("dbo.UserPhones", new[] { "Phone_Id" });
            DropIndex("dbo.UserAddresses", new[] { "User_Id" });
            DropIndex("dbo.UserAddresses", new[] { "Address_Id" });
            DropIndex("dbo.Users", new[] { "Login_Id" });
            DropIndex("dbo.ProductSizes", new[] { "Size_Id" });
            DropIndex("dbo.ProductSizes", new[] { "Product_Id" });
            DropIndex("dbo.ProductImages", new[] { "Product_Id" });
            DropIndex("dbo.ProductImages", new[] { "Image_Id" });
            DropTable("dbo.UserPhones");
            DropTable("dbo.UserAddresses");
            DropTable("dbo.Users");
            DropTable("dbo.ProductTags");
            DropTable("dbo.Sizes");
            DropTable("dbo.ProductSizes");
            DropTable("dbo.ProductImages");
            DropTable("dbo.Products");
            DropTable("dbo.Phones");
            DropTable("dbo.Logins");
            DropTable("dbo.Images");
            DropTable("dbo.Addresses");
        }
    }
}
