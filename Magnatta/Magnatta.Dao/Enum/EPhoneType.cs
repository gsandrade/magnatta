﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Enum
{
	public enum EPhoneType
	{
		HOMEPHONE = 1,
		CELLPHONE = 2,
		BUSINESSPHONE = 3
	}
}
