using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Magnatta.Dao.Entities.User;
using Magnatta.Dao.Entities.Product;
using Magnatta.Dao.Entities.Image;

namespace Magnatta.Dao.Context
{
	public partial class Context : DbContext
	{
		public Context() : base("strConnection") { }
		#region Users
		public DbSet<User> User { get; set; }
		public DbSet<Login> Login { get; set; }
		public DbSet<UserAddress> UserAddress { get; set; }
		public DbSet<Address> Address { get; set; }
		public DbSet<UserPhone> UserPhone { get; set; }
		public DbSet<Phone> Phone { get; set; }
		#endregion

		#region Products
		public DbSet<Product> Product { get; set; }
		public DbSet<ProductSize> ProductSize { get; set; }
		public DbSet<Size> Size { get; set; }
		public DbSet<ProductTag> ProductTag { get; set; }
		#endregion

		#region Image
		public DbSet<Image> Image { get; set; }
		public DbSet<ProductImage> ProductImage { get; set; }
		#endregion
	}
}
