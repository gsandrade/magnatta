﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.Product
{
	public class Product : GenericEntity
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Material { get; set; }
		public string Price { get; set; }
		public string CssSize { get; set; }
		public int Size { get; set; }
		public int Ordem { get; set; }
		public virtual IEnumerable<Image.Image> Images { get; set; }
		public virtual IEnumerable<Size> Sizes { get; set; }
		public virtual IEnumerable<ProductTag> ProductTags { get; set; }
	}
}
