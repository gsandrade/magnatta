﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.Product
{
	/// <summary>
	/// Toda as medidas em CMs
	/// </summary>
	public class Size : GenericEntity
	{
		public string Name { get; set; }
		public decimal SleeveWidth { get; set; }
		public decimal SleeveLength { get; set; }
		public decimal Shoulder { get; set; }
		public decimal Collar { get; set; }
		public decimal Height { get; set; }
		public decimal Width { get; set; }
		public int Ordem { get; set; }
	}
}
