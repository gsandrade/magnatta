﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.Product
{
	public class ProductSize : GenericEntity
	{
		public Size Size { get; set; }
		public Product Product { get; set; }
		public int QuantityInStock { get; set; }
	}
}
