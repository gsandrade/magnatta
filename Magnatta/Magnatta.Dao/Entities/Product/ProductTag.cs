﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.Product
{
	public class ProductTag : GenericEntity
	{
		public string Name { get; set; }
		public int ProductId { get; set; }
	}
}
