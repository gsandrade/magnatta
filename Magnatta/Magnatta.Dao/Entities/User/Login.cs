﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.User
{
	public class Login : GenericEntity
	{
		public string User { get; set; }
		public string Password { get; set; }
	}
}
