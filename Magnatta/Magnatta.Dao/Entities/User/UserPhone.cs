﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.User
{
	public class UserPhone : GenericEntity
	{
		public User User { get; set; }
		public Phone Phone { get; set; }
	}
}
