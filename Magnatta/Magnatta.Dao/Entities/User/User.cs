﻿using Magnatta.Dao.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.User
{
	public class User : GenericEntity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public ESextType Sex { get; set; }	
		public DateTime BirthDate { get; set; }
		public string Email { get; set; }
		public virtual Login Login { get; set; }
	}
}
