﻿using Magnatta.Dao.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.User
{
	public class Phone : GenericEntity
	{
		public string IDD { get; set; }
		public string DDD { get; set; }
		public string Number { get; set; }
		public string Extension { get; set; }
		public EPhoneType Type { get; set; }
	}
}
