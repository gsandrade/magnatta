﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.User
{
	public class UserAddress : GenericEntity
	{
		public User User { get; set; }
		public Address Address { get; set; }
	}
}
