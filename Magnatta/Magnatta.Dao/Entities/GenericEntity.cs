﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities
{
	public class GenericEntity
	{
		[Key]
		public Guid Id { get; set; }
	}
}
