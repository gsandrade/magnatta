﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.Image
{
	public class Image : GenericEntity
	{
		public string Path { get; set; }
		public string Title { get; set; }
	}
}
