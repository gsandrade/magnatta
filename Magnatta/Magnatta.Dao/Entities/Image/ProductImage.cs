﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Dao.Entities.Image
{
	public class ProductImage : GenericEntity
	{
		public Product.Product Product { get; set; }
		public Image Image { get; set; }
	}
}
