﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Biz.Enum
{
	public enum EMEnu
	{
		HOME = 1,
		MASCULINO = 2,
		FEMININO = 3,
		ACESSORIOS = 4,
		CARRINHO = 5,
		ACESSO = 6
	}
}
