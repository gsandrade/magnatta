﻿using Magnatta.Biz.VO;
using Magnatta.Dao.Enum;
using Magnatta.Dao.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Biz
{
	public class UserBiz
	{
		private UserRepository _repo;
		public UserBiz()
		{
			_repo = new UserRepository();
		}

		public bool SaveUser(VO.User user){
			var us = Clone(user);
			us.Id = Guid.NewGuid();
			us.Login.Id = Guid.NewGuid();
			var newUser = _repo.Create(us);
			return true;
		}

		private Dao.Entities.User.User Clone(VO.User user){
			var result = new Dao.Entities.User.User();
			result.BirthDate = DateTime.Now;
			result.FirstName = user.FirstName;
			result.LastName = user.LastName;
			result.Sex = (ESextType)user.Sex;
			result.Email = user.Email;
			result.Login = new Dao.Entities.User.Login();
			result.Login.User = user.Email;
			result.Login.Password = Base64Encode(user.Password);
			return result;
		}

		public static string Base64Encode(string plainText)
		{
			var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
			return System.Convert.ToBase64String(plainTextBytes);
		}
	}
}
