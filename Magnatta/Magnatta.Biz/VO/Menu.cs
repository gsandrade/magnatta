﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Biz.VO
{
    public class Menu : GenericVO
    {
        public Guid? IdPai { get; set; }
        public string Nome { get; set; }
        public bool Visivel { get; set; }
        public string Href { get; set; }
        public string Descricao { get; set; }
        public int Ordem { get; set; }
        public string Target { get; set; }
        public IEnumerable<Menu> Filhos { get; set; }
    }
}
