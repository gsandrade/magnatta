﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Biz.VO
{
	public class Product : GenericVO
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public string Material { get; set; }
		public string Price { get; set; }
		public string CssSize { get; set; }
		public int ScreenSize { get; set; }
		public virtual IEnumerable<string> ImagePath { get; set; }
		public virtual IEnumerable<string> ProductSize { get; set; }
	}
}
