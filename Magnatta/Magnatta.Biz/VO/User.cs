﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Biz.VO
{
	public class User
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int Sex { get; set; }
		public string BirthDate { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
	}
}
