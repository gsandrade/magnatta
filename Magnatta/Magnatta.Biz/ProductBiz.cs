﻿using Magnatta.Dao.Entities.Product;
using Magnatta.Dao.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magnatta.Biz
{
	public class ProductBiz
	{
		private ProductRepository _repo;
		public ProductBiz()
		{
			_repo = new ProductRepository();
		}

		public IEnumerable<VO.Product> GetProductsInStock(string name = "")
		{
			IEnumerable<Product> products = null;
			if(!string.IsNullOrEmpty(name))
				products = _repo.GetAllInStock(name);
			else
				products = _repo.GetAllInStock();
			var result = products.Select(p => Clone(p)).ToList();
			return result;
		}
		private VO.Product Clone(Product item)
		{
			VO.Product result = new VO.Product();
			result.Id = item.Id;
			result.Name = item.Name;
			result.Description = item.Description;
			result.Material = item.Material;
			result.Price = item.Price;
			result.CssSize = item.CssSize;
			result.ScreenSize = item.Size;
			if (item.Images != null && item.Images.Count() > 0) result.ImagePath = item.Images.Select(x => x.Path);
			if (item.Sizes != null && item.Sizes.Count() > 0) result.ProductSize = item.Sizes.Select(x => x.Name);
			else result.ImagePath = new List<string>();
			return result;
		}

		public VO.Product GetProduct(Guid id)
		{
			return Clone(_repo.Get(id));
		}
	}
}
