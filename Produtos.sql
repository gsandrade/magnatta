use EasyPizza
go

begin tran
-- commit tran
-- rollback tran

delete from ProductImages
delete from ProductSizes
delete from Sizes
delete from Images
delete from Products

declare @productId UniqueIdentifier
declare @image1 UniqueIdentifier set @image1 = NEWID()
declare @image2 UniqueIdentifier set @image2 = NEWID()
declare @image3 UniqueIdentifier set @image3 = NEWID()
declare @image4 UniqueIdentifier set @image4 = NEWID()
declare @image5 UniqueIdentifier set @image5 = NEWID()
declare @image6 UniqueIdentifier set @image6 = NEWID()

INSERT INTO Images (Id, Path, Title) VALUES(@image1, '~/assets/img/Products/shirt1.jpg', '')
INSERT INTO Images (Id, Path, Title) VALUES(@image2, '~/assets/img/Products/shirt2.jpg', '')
INSERT INTO Images (Id, Path, Title) VALUES(@image3, '~/assets/img/Products/shirt3.jpg', '')
INSERT INTO Images (Id, Path, Title) VALUES(@image4, '~/assets/img/Products/shirt4.jpg', '')
INSERT INTO Images (Id, Path, Title) VALUES(@image5, '~/assets/img/Products/shirt5.jpg', '')
INSERT INTO Images (Id, Path, Title) VALUES(@image6, '~/assets/img/Products/shirt6.jpg', '')

declare @size1 UniqueIdentifier set @size1 = NEWID()
declare @size2 UniqueIdentifier set @size2 = NEWID()
declare @size3 UniqueIdentifier set @size3 = NEWID()
declare @size4 UniqueIdentifier set @size4 = NEWID()

INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width, Ordem) VALUES(@size1, 'P', 68, 100, 45, 15, 10, 10, 5)
INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width, Ordem) VALUES(@size2, 'M', 70, 102, 47, 15, 10, 10, 10)
INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width, Ordem) VALUES(@size3, 'G', 72, 104, 49, 15, 10, 10, 15)
INSERT INTO Sizes (Id, Name, SleeveWidth, SleeveLength, Shoulder, Collar, Height, Width, Ordem) VALUES(@size4,'GG', 74, 106, 51, 15, 10, 10, 20)

set @productId = NEWID()
INSERT INTO Products (Id, Name, Description, Material, Price, CssSize, Size, Ordem) VALUES(@productId, 'Produto 01', 'Sem descricao', '100% Algodao', 103.4, 'col-sm-8', 8, 5)
INSERT INTO ProductImages(Id, Product_Id, Image_Id) VALUES(NEWID(), @productId, @image1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size4)

set @productId = NEWID()
INSERT INTO Products (Id, Name, Description, Material, Price, CssSize, Size, Ordem) VALUES(@productId, 'Produto 02', 'Sem descricao', '100% Algodao', 103.4, 'col-sm-4', 4, 10)
INSERT INTO ProductImages(Id, Product_Id, Image_Id) VALUES(NEWID(), @productId, @image2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size4)

set @productId = NEWID()
INSERT INTO Products (Id, Name, Description, Material, Price, CssSize, Size, Ordem) VALUES(@productId, 'Produto 03', 'Sem descricao', '100% Algodao', 103.4, 'col-sm-4', 4, 15)
INSERT INTO ProductImages(Id, Product_Id, Image_Id) VALUES(NEWID(), @productId, @image3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size4)

set @productId = NEWID()
INSERT INTO Products (Id, Name, Description, Material, Price, CssSize, Size, Ordem) VALUES(@productId, 'Produto 04', 'Sem descricao', '100% Algodao', 103.4, 'col-sm-12', 12, 20)
INSERT INTO ProductImages(Id, Product_Id, Image_Id) VALUES(NEWID(), @productId, @image4)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size4)

set @productId = NEWID()
INSERT INTO Products (Id, Name, Description, Material, Price, CssSize, Size, Ordem) VALUES(@productId, 'Produto 05', 'Sem descricao', '100% Algodao', 103.4, 'col-sm-12', 12, 25)
INSERT INTO ProductImages(Id, Product_Id, Image_Id) VALUES(NEWID(), @productId, @image5)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size4)

set @productId = NEWID()
INSERT INTO Products (Id, Name, Description, Material, Price, CssSize, Size, Ordem) VALUES(@productId, 'Produto 06', 'Sem descricao', '100% Algodao', 103.4, 'col-sm-8', 8, 30)
INSERT INTO ProductImages(Id, Product_Id, Image_Id) VALUES(NEWID(), @productId, @image6)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size1)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size2)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size3)
INSERT INTO ProductSizes(Id, QuantityInStock, Product_Id, Size_Id) VALUES(NEWID(), 5, @productId, @size4)

select * from Products
select * from Sizes
select * from Images
select * from ProductImages
select * from ProductSizes